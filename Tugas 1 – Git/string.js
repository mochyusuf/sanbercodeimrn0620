var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';

var string = word +' '+ second +' '+ third +' '+ fourth +' '+ fifth +' '+ sixth +' '+ seventh;
console.log(string)

var sentence = "I am going to be React Native Developer"; 

var word_from_sentence = sentence.split(" ");

var exampleFirstWord = sentence[0] ; 
var exampleSecondWord = sentence[2] + sentence[3]  ; 
var secondWord = word_from_sentence[1]; // lakukan sendiri 
var thirdWord = word_from_sentence[2]; // lakukan sendiri 
var fourthWord = word_from_sentence[3]; // lakukan sendiri 
var fifthWord = word_from_sentence[4]; // lakukan sendiri 
var sixthWord = word_from_sentence[5]; // lakukan sendiri 
var seventhWord = word_from_sentence[6]; // lakukan sendiri 
var eighthWord = word_from_sentence[7]; // lakukan sendiri 

console.log('First Word: ' + exampleFirstWord); 
console.log('Second Word: ' + secondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 
console.log('Eighth Word: ' + eighthWord);

var sentence2 = 'wow JavaScript is so cool'; 

var exampleFirstWord2 = sentence2.substring(0, 3); 
var secondWord2 = sentence2.substring(4, 14);  // do your own! 
var thirdWord2 = sentence2.substring(15, 17);  // do your own! 
var fourthWord2 = sentence2.substring(18, 20);  // do your own! 
var fifthWord2 = sentence2.substring(21, 25);  // do your own! 

console.log('First Word: ' + exampleFirstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);

var sentence3 = 'wow JavaScript is so cool'; 

var exampleFirstWord3 = sentence2.substring(0, 3); 
var secondWord3 = sentence2.substring(4, 14);  // do your own! 
var thirdWord3 = sentence2.substring(15, 17);  // do your own! 
var fourthWord3 = sentence2.substring(18, 20);  // do your own! 
var fifthWord3 = sentence2.substring(21, 25);  // do your own! 

var firstWordLength = exampleFirstWord3.length  
var secondWord2dLength = secondWord3.length  
var thirdWord2Length = thirdWord3.length  
var fourthWord2Length = fourthWord3.length  
var fifthWord2Length = fifthWord3.length  
// lanjutkan buat variable lagi di bawah ini 
console.log('First Word: ' + exampleFirstWord3 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWord2dLength); 
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWord2Length); 
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWord2Length);  
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWord2Length);  